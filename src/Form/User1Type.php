<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class User1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('email')
        ->add('password',null,['label'=>'Mot de passe'])
        ->add('lastName',null,['label' => 'Prénom'])
        ->add('firstName',null,['label' => 'Nom'])
        ->add('sexe',ChoiceType::class, [
            'label' => 'genre',
            'choices' => [
                "choose your gender male | female" => [
                    'male' => "male",
                    'female' => "female"
                ]
            ]
        ])
        ->add('dateOfBirth',DateType::class, [
            'label' => 'Date de naissance',
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('address',null,['label' => 'Adresse'])
            ->add('cadeau')
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices' => [
                    "ADMIN" => "ROLE_ADMIN",
                    "GESTIONNAIRE" => "ROLE_GESTIONNAIRE",
                    "SECRETARIAT" => "ROLE_SECRETARIAT",
                    "CLIENT" => "ROLE_CLIENT",
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
