<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName',null,['label' => 'Prénom'])
            ->add('firstName',null,['label' => 'Nom'])
            ->add('email')
            ->add('password',null,['label'=>'Mot de passe'])
            ->add('sexe',ChoiceType::class, [
                'label' => 'genre',
                'choices' => [
                    "choose your gender male | female" => [
                        'male' => "male",
                        'female' => "female"
                    ]
                ]
            ])
            ->add('dateOfBirth',DateType::class, [
                'label' => 'Date de naissance',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('address',null,['label' => 'Adresse'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
