<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Cadeau::class, mappedBy="category", orphanRemoval=true)
     */
    private $cadeau;

    public function __construct()
    {
        $this->cadeau = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Cadeau[]
     */
    public function getCadeau(): Collection
    {
        return $this->cadeau;
    }

    public function addCadeau(Cadeau $cadeau): self
    {
        if (!$this->cadeau->contains($cadeau)) {
            $this->cadeau[] = $cadeau;
            $cadeau->setCategory($this);
        }

        return $this;
    }

    public function removeCadeau(Cadeau $cadeau): self
    {
        if ($this->cadeau->removeElement($cadeau)) {
            // set the owning side to null (unless already changed)
            if ($cadeau->getCategory() === $this) {
                $cadeau->setCategory(null);
            }
        }

        return $this;
    }

    public function __toString():string
    {
        return $this->getName();
    }
}
