<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
    
    /**
     * @Route("/inscription", name="inscription", methods={"GET"})
     */
    public function inscription()
    {

        $genreType = ["male"=>"male","female"=>"female"];
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('inscription_add'))
            ->setMethod('POST')
            ->add('nom')
            ->add('prenom')
            ->add('email',EmailType::class)
            ->add('dateDeNaissance',DateType::class,
                ['label' => 'date de naissance',
                    'widget'=> 'single_text',
                    'format' => 'yyyy-MM-dd',
                ])
            ->add('genre',ChoiceType::class,[
                'choices' => $genreType,
                'expanded' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])

            ->add('adresse',EntityType::class, [
                'class' => Address::class])
            ->add('password', PasswordType::class)

            ->add('inscrire', SubmitType::class)
            ->getForm();

        return $this->render('security/inscription.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/inscription/add", name="inscription_add", methods={"POST"})
     */
    public function ajoutInscription(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = $request->request->get("form");
        $adresse = $this->getDoctrine()->getRepository(Address::class)->find($data["adresse"]);
        $user = new User();

        $user
            ->setFirstName($data['nom'])
            ->setLastName($data['prenom'])
            ->setEmail($data["email"])
            ->setAddress($adresse)
            ->setDateOfBirth(new \DateTime($data['dateDeNaissance']))
            ->setSexe($data["genre"])
            ->setRoles(['ROLE_CLIENT'])
            ->setPassword($encoder->encodePassword($user, $data['password']))

        ;
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('app_login');
    }
}
