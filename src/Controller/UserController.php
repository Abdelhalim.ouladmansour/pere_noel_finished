<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\User1Type;
use App\Repository\UserRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET", "POST"})
     */
    public function index(UserRepository $userRepository, Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('rue',TextType::class,[
                'required' => false,
            ])
            ->add('search', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $rue = $form->getData()['rue'];

            $users  = $this->getDoctrine()->getRepository(User::class)->findAll();
            $tabUser = [];
            if($rue != "") {
                $i = 0;
                foreach ($users as $user) {
                    if ($user->getAddress()->getRue() == $rue) {
                        $tabUser[$i] = $user;
                        $i++;
                    }
                }
            } else {
                $tabUser = $users;
            }
            return $this->render('user/index.html.twig', [
                'users' => $tabUser,
                'form' => $form->createView()
            ]);
        }
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(User1Type::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

     /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createFormBuilder($user)
        ->add('password', null, ['label'=>'Mot de passe'])
        ->add('sexe',ChoiceType::class, [
            'label' => 'genre',
            'choices' => [
                "choose your gender male | female" => [
                    'male' => "male",
                    'female' => "female"
                ]
            ]
        ])
        ->add('dateOfBirth',DateType::class, [
            'label' => 'Date de naissance',
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ])
        ->add('address',null,['label' => 'Adresse'])
        ->getForm()
        ;
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
