<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Cadeau;
use App\Entity\User;

class BasketController extends AbstractController
{
    /**
     * @Route("/basket", name="basket")
     */
    public function mesBasket(): Response
    {
        $user = $this->getUser();
        //$user = $this->getUser();
        $cadeaux = $user->getCadeau();
        return $this->render('basket/index.html.twig', [
            'mesBaskets' => $cadeaux,
        ]);
    }
    
    /**
     * @Route("/basket/cadeaux", name="basket_all_cadeaux")
     */
    public function allCadeaux(): Response
    {
        $cadeaux = $this->getDoctrine()->getRepository(Cadeau::class)->findAll();
        //$user = $this->getUser();
        return $this->render('basket/listCadeuPourSouhait.html.twig', [
            'baskets' => $cadeaux,
        ]);
    }

    /**
     * @Route("/basket/cadeau/{id}", name="souhaitAdd")
     */
    public function Addsouahit(Cadeau $cadeau): Response
    {
        $user = $this->getUser();
        //$user = $this->getUser();
        $user->addCadeau($cadeau);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute("basket");
    }
    /**
     * @Route("/basket/cadeau/{id}/delete", name="souhaitDelete")
     */
    public function deleteSouahit(Cadeau $cadeau): Response
    {
        $user = $this->getUser();
        $user->removeCadeau($cadeau);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('basket');
    }
}
